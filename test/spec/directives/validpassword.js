'use strict';

describe('Directive: validPassword', function () {

  // load the directive's module
  beforeEach(module('lasaFrontendApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<valid-password></valid-password>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the validPassword directive');
  }));
});
