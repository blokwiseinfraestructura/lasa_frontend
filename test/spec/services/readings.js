'use strict';

describe('Service: readings', function () {

  // load the service's module
  beforeEach(module('lasaFrontendApp'));

  // instantiate service
  var readings;
  beforeEach(inject(function (_readings_) {
    readings = _readings_;
  }));

  it('should do something', function () {
    expect(!!readings).toBe(true);
  });

});
