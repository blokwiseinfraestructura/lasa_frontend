'use strict';

describe('Service: usersapp', function () {

  // load the service's module
  beforeEach(module('lasaFrontendApp'));

  // instantiate service
  var usersapp;
  beforeEach(inject(function (_usersapp_) {
    usersapp = _usersapp_;
  }));

  it('should do something', function () {
    expect(!!usersapp).toBe(true);
  });

});
