'use strict';

describe('Filter: questions', function () {

  // load the filter's module
  beforeEach(module('lasaFrontendApp'));

  // initialize a new instance of the filter before each test
  var questions;
  beforeEach(inject(function ($filter) {
    questions = $filter('questions');
  }));

  it('should return the input prefixed with "questions filter:"', function () {
    var text = 'angularjs';
    expect(questions(text)).toBe('questions filter: ' + text);
  });

});
