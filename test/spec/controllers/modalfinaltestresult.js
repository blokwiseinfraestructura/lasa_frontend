'use strict';

describe('Controller: ModalfinaltestresultCtrl', function () {

  // load the controller's module
  beforeEach(module('lasaFrontendApp'));

  var ModalfinaltestresultCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalfinaltestresultCtrl = $controller('ModalfinaltestresultCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ModalfinaltestresultCtrl.awesomeThings.length).toBe(3);
  });
});
