'use strict';

describe('Controller: ModaldetailreadingsCtrl', function () {

  // load the controller's module
  beforeEach(module('lasaFrontendApp'));

  var ModaldetailreadingsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModaldetailreadingsCtrl = $controller('ModaldetailreadingsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ModaldetailreadingsCtrl.awesomeThings.length).toBe(3);
  });
});
