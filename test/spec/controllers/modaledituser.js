'use strict';

describe('Controller: ModaledituserCtrl', function () {

  // load the controller's module
  beforeEach(module('lasaFrontendApp'));

  var ModaledituserCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModaledituserCtrl = $controller('ModaledituserCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ModaledituserCtrl.awesomeThings.length).toBe(3);
  });
});
