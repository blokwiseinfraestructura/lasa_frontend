'use strict';

describe('Controller: ModaleditreadingsCtrl', function () {

  // load the controller's module
  beforeEach(module('lasaFrontendApp'));

  var ModaleditreadingsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModaleditreadingsCtrl = $controller('ModaleditreadingsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ModaleditreadingsCtrl.awesomeThings.length).toBe(3);
  });
});
