'use strict';

describe('Controller: UsersappdetailCtrl', function () {

  // load the controller's module
  beforeEach(module('lasaFrontendApp'));

  var UsersappdetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UsersappdetailCtrl = $controller('UsersappdetailCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(UsersappdetailCtrl.awesomeThings.length).toBe(3);
  });
});
