'use strict';

describe('Controller: PushnotificationsCtrl', function () {

  // load the controller's module
  beforeEach(module('lasaFrontendApp'));

  var PushnotificationsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PushnotificationsCtrl = $controller('PushnotificationsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PushnotificationsCtrl.awesomeThings.length).toBe(3);
  });
});
