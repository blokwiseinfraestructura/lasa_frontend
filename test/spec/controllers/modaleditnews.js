'use strict';

describe('Controller: ModaleditnewsCtrl', function () {

  // load the controller's module
  beforeEach(module('lasaFrontendApp'));

  var ModaleditnewsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModaleditnewsCtrl = $controller('ModaleditnewsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ModaleditnewsCtrl.awesomeThings.length).toBe(3);
  });
});
