'use strict';

describe('Controller: ReadingsCtrl', function () {

  // load the controller's module
  beforeEach(module('lasaFrontendApp'));

  var ReadingsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReadingsCtrl = $controller('ReadingsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ReadingsCtrl.awesomeThings.length).toBe(3);
  });
});
