'use strict';

describe('Controller: ModaldetailnewsCtrl', function () {

  // load the controller's module
  beforeEach(module('lasaFrontendApp'));

  var ModaldetailnewsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModaldetailnewsCtrl = $controller('ModaldetailnewsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ModaldetailnewsCtrl.awesomeThings.length).toBe(3);
  });
});
