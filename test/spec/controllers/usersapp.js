'use strict';

describe('Controller: UsersappCtrl', function () {

  // load the controller's module
  beforeEach(module('lasaFrontendApp'));

  var UsersappCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UsersappCtrl = $controller('UsersappCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(UsersappCtrl.awesomeThings.length).toBe(3);
  });
});
