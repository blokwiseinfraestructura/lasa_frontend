'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('AboutCtrl', function ($scope, answers, $uibModalInstance, questions) {
    $scope.answers = answers;
    $scope.saving = false;
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

  $scope.editQuestion = function (question, answer) {
    $scope.saving = true;
    questions.editQuestion(question, answer, answers.data.questionId).then(function successCallback(response) {
      $scope.saving = false;
      $uibModalInstance.close(response.data);
    }, function errorCallback(response) {

    });
  };

  });
