'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:UsersappdetailCtrl
 * @description
 * # UsersappdetailCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('UsersappdetailCtrl', function ($scope, $routeParams, $uibModal, usersapp) {

    $scope.loanding = true;
    $scope.saving = false;
    $scope.userDetailData = {};
    $scope.levels = [
      {
        levelId: 1,
        levelDescription: "Nivel 1",
        levelCode: 1
      },
      {
        levelId: 2,
        levelDescription: "Nivel 2",
        levelCode: 2
      },
      {
        levelId: 3,
        levelDescription: "Nivel 3",
        levelCode: 3
      },
      {
        levelId: 4,
        levelDescription: "Nivel 4",
        levelCode: 4
      },
      {
        levelId: 5,
        levelDescription: "Nivel 5",
        levelCode: 5
      }, {
        levelId: 6,
        levelDescription: "Nivel 6",
        levelCode: 6
      },
      {
        levelId: 7,
        levelDescription: "Nivel 7",
        levelCode: 7
      },
      {
        levelId: 8,
        levelDescription: "Nivel 8",
        levelCode: 8
      },
      {
        levelId: 9,
        levelDescription: "Evaluacion Final",
        levelCode: 9
      }
    ];
    $scope.userLevels = [];

    usersapp.getUserDetails($routeParams).then(function successCallback(response) {
      if (response.data.status) {
        $scope.userDetailData = {
          userData: response.data.userData,
          userStatistics: response.data.userStatistics,
          userLevel: response.data.userLevel,
          userAppModulesProgressArray: response.data.userAppModulesProgressArray
        };
        Highcharts.chart('container', {
          chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
          },
          title: {
            text: 'Duelos'
          },
          credits: {
            enabled: false
          },
          plotOptions: {
            pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                enabled: false
              },
              showInLegend: true
            }
          },
          series: [
            {
              name: 'Cantidad',
              colorByPoint: true,
              data: [
                {
                  name: 'Ganados',
                  y: $scope.userDetailData.userStatistics.challengesWon
                },
                {
                  name: 'Perdidos',
                  y: $scope.userDetailData.userStatistics.challengesLost,
                  sliced: true,
                  selected: true
                }
              ]
            }
          ]
        });
        angular.forEach($scope.levels, function (valueLevel, keyLevel) {
          var levelModules = [];
          var finalAverage = 0;
          angular.forEach($scope.userDetailData.userAppModulesProgressArray, function (value, key) {
            if (value.appModule.level.levelId === valueLevel.levelId) {
              if (value.appModule.level.levelId === 9){
                finalAverage = (finalAverage + value.score)/2;
              }
              levelModules.unshift(value);
            } else {
              /*levelModules.push( {
               appModule: {
               level: valueLevel
               },
               score: 0,
               maxScore: 0
               });*/
            }
          });
          $scope.userLevels.push({
            finalAverage: finalAverage,
            level: valueLevel,
            modules: levelModules
          });
        });
        $scope.loanding = false;
      } else {
        $scope.loanding = false;
      }
    }, function errorCallback(response) {
      console.log(response);
    });

    $scope.enableDisabled = function (state) {
      swal({
        title: "Advertencia",
        text: ((state) ? "Esta seguro que desea Inhabilitar Evaluación final" : "Esta seguro que desea Habilitar Evaluación final"),
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
      }, function () {
        setTimeout(function () {
          usersapp.changeEvaluation($scope.userDetailData.userData, ((state) ? false : true)).then(function successCallback(response) {
            if (response.data.status) {
              $scope.userDetailData.userData.haveFinalTestAccess = response.data.userData.haveFinalTestAccess;
              swal("Actualizado", ((state) ? "La evaluacion se a inhabilitado correctamente" : "La evaluacion se a habilitado correctamente"), "success");
            } else {
              swal("Error", "hubo un error al actualizar el registro", "error");
            }
          }, function errorCallback(response) {
            swal("Error", "Debe ingresar un total de cuatro respuestas", "error");
          });
        }, 2000);
      });
    };

    $scope.sendEmail = function (emailData) {
      $scope.saving = true;
      usersapp.sendEmail(emailData, $scope.userDetailData.userData).then(function successCallback(response) {
        if (response.data.status) {
          $scope.email = angular.copy({});
          $scope.formEmail.$setPristine();
          $('#exampleModal').modal('hide');
          $scope.saving = false;
          swal("Enviado", "Su mensaje a sido enviado correctamenet", "success");
        } else {
          $scope.saving = false;
          swal("Error", "Hubo un erro al enviar el mensaje compruebe el formulario y vuelva a intentarlo", "error");
        }
      }, function errorCallback(response) {
        $scope.saving = false;
        swal("Error", "Hubo un error al conectarse con el servidor compruebe su conexion a internet", "error");
      });
    };

    $scope.modalFinalTestResult = function (resultData) {
      $uibModal.open({
        templateUrl: 'views/modalfinaltestresult.html',
        controller: 'ModalfinaltestresultCtrl',
        controllerAs: 'modalFinalTestResult',
        size: 'md',
        resolve: {
          finalTestResult: function () {
            return resultData;
          }
        }
      }).result.then(function () {
        },function () {
        }
      );
    };
  });
