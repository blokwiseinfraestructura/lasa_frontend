'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:ModalfinaltestresultCtrl
 * @description
 * # ModalfinaltestresultCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('ModalfinaltestresultCtrl', function ($scope, finalTestResult, $uibModalInstance) {
    var totalHits = 0;
    var totalMistakes = 0;
    $scope.finalTestResults = angular.copy(finalTestResult);

    angular.forEach($scope.finalTestResults, function (value, key) {
      totalHits = totalHits + value.rightAnswers;
      totalMistakes = totalMistakes + value.wrongAnswers;
    });

    $scope.finalTestResults.push({
        level: {
          levelDescription: "Total"
        },
        rightAnswers: totalHits,
        wrongAnswers: totalMistakes
      }
    );


    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
