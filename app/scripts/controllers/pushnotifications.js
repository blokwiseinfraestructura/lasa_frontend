'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:PushnotificationsCtrl
 * @description
 * # PushnotificationsCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('PushnotificationsCtrl', function ($scope, pushNotifications) {

    $scope.loanding = true;
    $scope.saving = false;
    $scope.notifications = [];

    //Valores del paginado
    $scope.maxSize = 5;
    $scope.currentPage = 4;
    $scope.pageSize = 10;

    $scope.sendNotifications = function (dataPush) {
      var userData = JSON.parse(localStorage.getItem('CS_US'));
      $scope.saving = true;
      pushNotifications.addNewPushNotification(userData, dataPush).then(function successCallback(response) {
        if (response.data.status) {
          $scope.notifications.unshift(response.data);
          $scope.notification = angular.copy({});
          $scope.formNotifications.$setPristine();
          $('#registerModal').modal('hide');
          $scope.saving = false;
          swal("Enviado", "Su notificacion a sido enviada correctamente", "success");
        } else {
          swal("Error", "hubo un error al enviar la notificacion vuelva a intentarlo", "error");
          $scope.saving = false;
        }
      }, function errorCallback(response) {
        $scope.saving = false;
        swal("Error", "Hubo un error al conectarse con el servidor compruebe su conexion a internet", "error");
      });

    };

    pushNotifications.getAllNotifications().then(function successCallback(response) {
      if (response.data.status) {
        $scope.notifications = response.data.pushNotificationsArray;
        $scope.loanding = false;
      } else {
        $scope.loanding = false;
      }
    }, function errorCallback(response) {

    });


  });
