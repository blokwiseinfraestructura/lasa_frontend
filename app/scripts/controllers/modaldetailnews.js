'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:ModaldetailnewsCtrl
 * @description
 * # ModaldetailnewsCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('ModaldetailnewsCtrl', function ($scope, newsImage, $uibModalInstance) {
    $scope.newsData = newsImage;

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
