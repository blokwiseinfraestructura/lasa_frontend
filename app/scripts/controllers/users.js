'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('UsersCtrl', function ($scope, $uibModal, users) {

    $scope.loanding = true;
    $scope.saving = false;
    $scope.userModule = [
      {
        "moduleId": 1,
        "moduleName": "",
        "moduleCode": 1,
        "isAvailable": false
      },
      {
        "moduleId": 1,
        "moduleName": "",
        "moduleCode": 1,
        "isAvailable": false
      },
      {
        "moduleId": 2,
        "moduleName": "",
        "moduleCode": 2,
        "isAvailable": false
      },
      {
        "moduleId": 3,
        "moduleName": "",
        "moduleCode": 3,
        "isAvailable": false
      },
      {
        "moduleId": 4,
        "moduleName": "",
        "moduleCode": 4,
        "isAvailable": false
      },
      {
        "moduleId": 5,
        "moduleName": "",
        "moduleCode": 5,
        "isAvailable": false
      },
      {
        "moduleId": 6,
        "moduleName": "",
        "moduleCode": 6,
        "isAvailable": false
      }

    ];
    $scope.usersAdmins = [];

    $scope.addUser = function (user) {
      if ($scope.formUsers.$valid) {
        if (user.userPassword === user.passwordConfirm) {
          $scope.saving = true;
          users.addUser(user, $scope.userModule).then(function successCallback(response) {
            $scope.saving = false;
            if (response.data.status) {
              $scope.usersAdmins.unshift({modulesArray: response.data.modulesArray, userData: response.data.userData});
              $scope.user = angular.copy({});
              $scope.formUsers.$setPristine();
              $('#registerModal').modal('hide');
              swal("Guardado", "El usuario a sido guardado correctamente", "success");
            } else {
              //swal("Error", "Error al guardar el usuario", "error");
              swal("Error", response.data.mensaje, "error");
            }
          }, function errorCallback(response) {
            console.log(response);
          });

        } else {
          swal("Error", "Su Contraseña no coincide", "error");
        }
      }
    };

    users.getUser().then(function successCallback(response) {
      if (response.data.status) {
        $scope.usersAdmins = response.data.adminUsersArray;
        $scope.loanding = false;
      } else {
        swal("Error", "Error al guardar el usuario", "error");
      }
    }, function errorCallback(response) {
    });

    $scope.editUser = function (user) {
      var index = arrayObjectIndexOf($scope.usersAdmins, user);
      $uibModal.open({
        templateUrl:'views/modaledituser.html',
        controller:'ModaledituserCtrl',
        size: 'lg',
        resolve:{
          user: function () {
            return $scope.usersAdmins[index];
          }
        }
      }).result.then(function (data) {
        if (data.status){
          $scope.usersAdmins[index] = {modulesArray: data.modulesArray , userData: data.userData };
          swal("Modificado", "El registro a sido modificado correctamente", "success");
        }else {
          swal("Error", "Error al modificar el registro", "error");
        }
      },function () {

      });
    };
    
    $scope.deleteUser = function (user) {
      var index = arrayObjectIndexOf($scope.usersAdmins, user);
      swal({
        title: "Advertencia",
        text: "Esto no es reversible esta seguro de continuar",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
      }, function () {
        setTimeout(function () {
          users.deleteUser(user.userData.adminUserId).then(function successCallback(response) {
            if (response.data.status){
              $scope.usersAdmins.splice(index, 1);
              swal("Eliminado", "Su registro a sido eliminado correctamente", "success");
            }else{
              swal("Error", "hubo un error al eliminar el registro", "error");
            }
          }, function errorCallback(response) {
            swal("Error", "Error al hacer la peticion", "error");
          });
        }, 2000);
      });
    };


    //Optener el indice de la array de las preguntas
    function arrayObjectIndexOf(arr, obj){
      for(var i = 0; i < arr.length; i++){
        if(angular.equals(arr[i], obj)){
          return i;
        }
      }
      return -1;
    }

  });
