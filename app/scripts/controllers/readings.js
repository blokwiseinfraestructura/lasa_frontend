'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:ReadingsCtrl
 * @description
 * # ReadingsCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('ReadingsCtrl', function ($scope, $uibModal, readings) {

    $scope.loanding = true;
    $scope.saving = false;
    $scope.questionTopics = [];
    $scope.questionLevels = [];
    $scope.readingsArray = [];

    //Valores del paginado
    $scope.maxSize = 5;
    $scope.currentPage = 4;
    $scope.pageSize = 10;

    readings.getReadings().then(function successCallback(response) {
      $scope.questionTopics = response.data.topicsArray;
      $scope.questionLevels = response.data.levelsArray;
      if (response.data.status) {
        $scope.readingsArray = response.data.readingsArray;
        $scope.loanding = false;
      } else {
        $scope.readingsArray = [];
        $scope.loanding = false;
      }
    }, function errorCallback(response) {

    });

    $scope.addReading = function (readingData) {
      $scope.saving = true;
      readings.addReading(readingData).then(function successCallback(response) {
        $scope.saving = false;
        if (response.data.status) {
          $scope.readingsArray.unshift(response.data.reading);
          $scope.reading = angular.copy({});
          $scope.formAddReading.$setPristine();
          $('#image-preview').attr('src', '');
          $('#registerModal').modal('hide');
          swal("Guardado", "Su lectura a sido guardada correctamente", "success");
        } else {
          swal("Error", "Hubo un error al guardar el registro compruebe el formulario y vuelva a intentarlo", "error");
        }
      }, function errorCallback(response) {
        swal("Error", "Hubo un error al conectarse con el servidor compruebe su conexion a internet", "error");
        $scope.saving = false;
      });

    };

    $scope.detailReading = function (readingData) {
      var index = arrayObjectIndexOf($scope.readingsArray, readingData);
      $uibModal.open({
        templateUrl: 'views/modaldetailreadings.html',
        controller: 'ModaldetailreadingsCtrl',
        size: 'md',
        resolve: {
          readingImage: function () {
            return $scope.readingsArray[index];
          }
        }
      }).result.then(function () {

        }, function () {
        }
      );
    };

    $scope.editReading = function (readingData) {
      var index = arrayObjectIndexOf($scope.readingsArray, readingData);
      $uibModal.open({
        templateUrl:'views/modaleditreadings.html',
        controller:'ModaleditreadingsCtrl',
        size: 'md',
        resolve: {
          editReadingData: function () {
            return {
              topicArray: $scope.questionTopics,
              levelArray: $scope.questionLevels,
              readingData: $scope.readingsArray[index]
            };
          }
        }
      }).result.then(function (data) {
        if (data.status){
          $scope.readingsArray [index] = data.reading;
          console.log($scope.readingsArray);
          swal("Modificado", "El registro a sido modificado correctamente", "success");
        }else {
          swal("Error", "Error al modificar el registro compruebe el formulario y vuelva a intentarlo", "error");
        }
      },function () {

      });
    };

    $scope.deleteReading = function (readingData) {
      var index = arrayObjectIndexOf($scope.readingsArray, readingData);
      swal({
        title: "Advertencia",
        text: "Esto no es revercible esta seguro de continuar",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
      }, function () {
        setTimeout(function () {
          readings.deleteReading(readingData).then(function successCallback(response) {
            if (response.data.status) {
              $scope.readingsArray.splice(index, 1);
              swal("Eliminado", "Su lectura a sido Eliminada correctamente", "success");
            } else {
              swal("Error", "Hubo un error al eliminar la lectura buelvalo a intentar", "error");
            }
          }, function errorCallback(response) {
            swal("Error", "Error al conectarse con el servidor verifique la conexion a internet", "error");
          });
        }, 2000);
      });
    };

    $("#image-picker-reading").change(function (event) {
      readURL(this);
    });

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#image-preview-reading').attr('src', e.target.result);
          $('#previewImageReading').hide();
        };

        reader.readAsDataURL(input.files[0]);
      }
    }

    //Optener el indice de la array
    function arrayObjectIndexOf(arr, obj) {
      for (var i = 0; i < arr.length; i++) {
        if (angular.equals(arr[i], obj)) {
          return i;
        }
      }
      return -1;
    }

  });

