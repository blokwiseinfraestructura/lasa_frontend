'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:QuestionsCtrl
 * @description
 * # QuestionsCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
    .controller('QuestionsCtrl', function ($scope, $uibModal, questions) {

        $scope.loanding = true;
        $scope.listAnswer = true;
        $scope.saving = false;
        $scope.questionTopics = [];
        $scope.questionLevels = [];
        $scope.questionsList = [];
        $scope.answersRequest = [];

        //Valores del paginado
        $scope.maxSize = 5;
        $scope.currentPage = 4;
        $scope.pageSize = 10;

        //Registra nueva pregunta
        $scope.newQuestion = function (question) {
            if ($scope.formQuestion.$valid) {
              if ($scope.answersRequest.length == 4){
                var questionCount = 0;
                angular.forEach($scope.answersRequest, function (value, key) {
                    if (value.isCorrect){
                      questionCount ++;
                    }
                });
                if (questionCount === 1){
                  $scope.saving = true;
                  questions.addNewQuestion(question, $scope.answersRequest).then(function successCallback(response) {
                    $scope.saving = false;
                    if (response.data.status){
                      $scope.questionsList.unshift(response.data.question);
                      $scope.question = angular.copy({});
                      $scope.formQuestion.$setPristine();
                      $scope.answersRequest.length = 0;
                      $scope.listAnswer = true;
                      $('#exampleModal').modal('hide');
                      swal("Guardado", "Su pregunta a sido guardada correctamente", "success");
                    }
                  }, function errorCallback(response) {
                    console.log(response);
                  });
                }else{
                  swal("Error", ((questionCount < 1) ? "Debe ingresar almenos una correcta" : "no puede ingresar mas de una respuesta correcta"), "error");
                }
              }else {
                swal("Error", "Debe ingresar un total de cuatro respuestas", "error");
              }
            }
        };

        //Consultar Nuevas preguntas
        questions.getAllQuestion().then(function successCallback(response) {
            $scope.questionsList = response.data.questionsArray;
            $scope.questionTopics = response.data.topicsArray;
            $scope.questionLevels = response.data.levelsArray;
            $scope.loanding = false;
        }, function errorCallback(response) {

        });

        //Ver las respuestas de las preguntas
        $scope.detailQuestion = function (question) {
          var index = arrayObjectIndexOf($scope.questionsList,question);
            $uibModal.open({
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl',
                size: 'md',
                resolve: {
                    answers: function () {
                        return $scope.questionsList[index];
                    }
                }
            }).result.then(function () {

                },function () {
                }
            );
        };

        //Ingresar Respuesta
      $scope.addAnswers = function (answer) {
        if ($scope.answersRequest.length < 4) {
          if (answer) {
            $scope.answersRequest.push({
              answerDescription: answer,
              isCorrect: false
            });
            $scope.listAnswer = false;
            $scope.question.answer = null;
          } else {
            swal("Error", "Ingrese una respuesta", "error");
          }

        } else {
          swal("Error", "Solo puede ingresar cuatro respuestas", "error");
        }
      };

        //Eliminar respuestas del formulario
        $scope.deleteAnswer = function (index) {
          $scope.answersRequest.splice(index,1);
          if($scope.answersRequest <= 0){
            $scope.listAnswer = true;
          }
        };

        $scope.deleteQuestion = function (id, question) {
          var index = arrayObjectIndexOf($scope.questionsList,question);
          swal({
            title: "Advertencia",
            text: "Esto no es revercible esta seguro de continuar",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
          }, function () {
            setTimeout(function () {
              questions.deleteQuestion(id).then(function successCallback(response) {
                  if (response.data.status){
                    $scope.questionsList.splice(index, 1);
                    swal("Eliminado", "Su pregunta a sido Eliminada correctamente", "success");
                  }else{
                    swal("Error", "Debe ingresar un total de cuatro respuestas", "error");
                  }
              }, function errorCallback(response) {
                swal("Error", "Debe ingresar un total de cuatro respuestas", "error");
              });
            }, 2000);
          });
        };

        $scope.editQuestion = function (question) {
          var index = arrayObjectIndexOf($scope.questionsList,question);
            $uibModal.open({
              templateUrl:'views/modaleditquestion.html',
              controller:'AboutCtrl',
              size: 'lg',
              resolve: {
                answers: function () {
                  return {
                    data:$scope.questionsList[index],
                    topics: $scope.questionTopics,
                    levels: $scope.questionLevels
                  };
                }
              }
            }).result.then(function (data) {
                if (data.status){
                  $scope.questionsList[index] = data.question;
                  swal("Modificado", "El registro a sido modificado correctamente", "success");
                }else {
                  swal("Error", "Error al modificar el registro", "error");
                }
            },function () {

            });
        };

      //Optener el indice de la array de las preguntas
      function arrayObjectIndexOf(arr, obj){
        for(var i = 0; i < arr.length; i++){
          if(angular.equals(arr[i], obj)){
            return i;
          }
        }
        return -1;
      }

    });
