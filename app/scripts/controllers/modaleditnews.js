'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:ModaleditnewsCtrl
 * @description
 * # ModaleditnewsCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('ModaleditnewsCtrl', function ($scope, editNewsData, $uibModalInstance, news) {
    console.log(editNewsData);

    $scope.newsData = editNewsData;
    $scope.saving = false;

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

    $scope.editNews = function (dataNews) {
      $scope.saving = true;
      news.editDataNews(dataNews, $scope.newsData).then(function successCallback(response) {
        $scope.saving = false;
        if (response.data.status) {
          $uibModalInstance.close(response.data);
        } else {
          swal("Error", "Error al modificar el usuario", "error");
        }
      }, function errorCallback(response) {
        swal("Error", "Hubo un error al conectarse con el servidor", "error");
      });
    };
  });
