'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:RecoverCtrl
 * @description
 * # RecoverCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('RecoverCtrl', function ($scope, $location, $routeParams, login) {

    $scope.loading = false;

    if (localStorage.getItem('CS_US') !== null) {
      localStorage.removeItem('CS_US');
      $location.path("/recover/"+$routeParams.token+"/"+$routeParams.isAdmin);
      location.reload();
    }

    $scope.restorePassword = function (dataUser) {
      if ($scope.formUserRecover.$valid) {
        if (dataUser.newPassword === dataUser.confirmPassword) {
          $scope.loading = true;
          login.restorePassword(dataUser.newPassword, $routeParams.token, (($routeParams.isAdmin === "1") ? true : false)).then(function successCallback(response) {
            $scope.loading = false;
            if (response.data.status) {
              swal("Buen trabajo", "la contraseña se a restablecido correctamente", "success");
              if ($routeParams.isAdmin === "1") {
                $location.path("/login");
              } else {
                $location.path("/loader");
              }
            } else {
              swal("Error", "Token Invalido", "error");
            }
          }, function errorCallback(response) {
            console.log(response);
          });
        } else {
          swal("Error", "Su Contraseña no coincide", "error");
        }
      }
    };

  });
