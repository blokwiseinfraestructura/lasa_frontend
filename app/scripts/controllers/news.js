'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:NewsCtrl
 * @description
 * # NewsCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('NewsCtrl', function ($scope, $uibModal, news) {

    $scope.saving = false;
    $scope.loanding = true;
    $scope.newsArray = [];

    //Valores del paginado
    $scope.maxSize = 5;
    $scope.currentPage = 4;
    $scope.pageSize = 10;

    $scope.addNews = function (dataNews) {
      $scope.saving = true;
      news.addNews(dataNews).then(function successCallback(response) {
        console.log(response);
        $scope.saving = false;
        if (response.data.status) {
          $scope.newsArray.unshift(response.data.news);
          $scope.news = angular.copy({});
          $scope.formAddNews.$setPristine();
          $('#image-preview').attr('src', '');
          $('#registerModal').modal('hide');
          swal("Guardado", "Su Noticia a sido guardado correctamente", "success");
        } else {
          swal("Error", "Com pruebe su formulario", "error");
        }
      }, function errorCallback(response) {
        swal("Error", "Hubo un error al conectarse con el servidor", "error");
      });
    };

    news.getNews().then(function successCallback(response) {
      if (response.data.status) {
        $scope.newsArray = response.data.newsArray;
        $scope.loanding = false;
      }else{
        swal("Error", "No hay noticias creadas", "error");
        $scope.loanding = false;
      }
    }, function errorCallback(response) {

    });

    $scope.deleteNews = function (newsData) {
      var index = arrayObjectIndexOf($scope.newsArray, newsData);
      swal({
        title: "Advertencia",
        text: "Esto no es revercible esta seguro de continuar",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
      }, function () {
        setTimeout(function () {
          news.deleteNews(newsData).then(function successCallback(response) {
            if (response.data.status) {
              $scope.newsArray.splice(index, 1);
              swal("Eliminado", "Su noticia a sido Eliminada correctamente", "success");
            } else {
              swal("Error", "Debe ingresar un total de cuatro respuestas", "error");
            }
          }, function errorCallback(response) {
            swal("Error", "Debe ingresar un total de cuatro respuestas", "error");
          });
        }, 2000);
      });
    };

    $scope.detailNews = function (newsData) {
      var index = arrayObjectIndexOf($scope.newsArray, newsData);
      $uibModal.open({
        templateUrl: 'views/modaldetailnews.html',
        controller: 'ModaldetailnewsCtrl',
        size: 'md',
        resolve: {
          newsImage: function () {
            return $scope.newsArray[index];
          }
        }
      }).result.then(function () {

        },function () {
        }
      );
    };

    $scope.editNews = function (newsData) {
      var index = arrayObjectIndexOf($scope.newsArray, newsData);
      $uibModal.open({
        templateUrl:'views/modaleditnews.html',
        controller:'ModaleditnewsCtrl',
        size: 'md',
        resolve: {
          editNewsData: function () {
            return $scope.newsArray[index];
          }
        }
      }).result.then(function (data) {
        if (data.status){
          $scope.newsArray [index] = data.news;
          swal("Modificado", "El registro a sido modificado correctamente", "success");
        }else {
          swal("Error", "Error al modificar el registro compruebe el formulario y vuelva a intentarlo", "error");
        }
      },function () {

      });
    };

    $("#image-picker").change(function (event) {
      readURL(this);
    });

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#image-preview').attr('src', e.target.result);
          $('#previewImage').hide();
        };

        reader.readAsDataURL(input.files[0]);
      }
    }

    //Optener el indice de la array
    function arrayObjectIndexOf(arr, obj) {
      for (var i = 0; i < arr.length; i++) {
        if (angular.equals(arr[i], obj)) {
          return i;
        }
      }
      return -1;
    }
  });
