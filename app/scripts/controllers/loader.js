'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:LoaderCtrl
 * @description
 * # LoaderCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('LoaderCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
