'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:ModaleditreadingsCtrl
 * @description
 * # ModaleditreadingsCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('ModaleditreadingsCtrl', function ($scope, editReadingData, $uibModalInstance, readings) {

    $scope.questionTopics = editReadingData.topicArray;
    $scope.questionLevels = editReadingData.levelArray;
    $scope.readingData = editReadingData.readingData;
    $scope.saving = false;

    $scope.EditReading = function (dataReading) {
      $scope.saving = true;
      readings.editReading(dataReading, $scope.readingData.readingId).then(function successCallback(response) {
        $scope.saving = false;
        if (response.data.status) {
          $uibModalInstance.close(response.data);
        } else {
          swal("Error", "Error al modificar el usuario revice el formulario y vuelva intentarlo", "error");
        }
      }, function errorCallback(response) {
        swal("Error", "Error al conectarse con el servidor verifique la conexion a internet", "error");
      });
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

  });
