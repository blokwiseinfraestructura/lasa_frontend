'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:ModaldetailreadingsCtrl
 * @description
 * # ModaldetailreadingsCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('ModaldetailreadingsCtrl', function ($scope, readingImage, $uibModalInstance) {

    $scope.reading = readingImage;
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

  });
