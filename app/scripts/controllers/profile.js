'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('ProfileCtrl', function ($scope, $rootScope, users) {

    $scope.visibleForm = false;
    $scope.savingAccess = false;
    $scope.validOldPassword = false;
    $scope.loandingPassword = false;
    $scope.user = JSON.parse(localStorage.getItem('CS_US'));

    $scope.saveUser = function () {
      users.editUser($scope.user, {
        modulesArray: $scope.user.modulesArray,
        userData: $scope.user.userData
      }, true).then(function successCallback(response) {
        if (response.data.status) {
          $rootScope.userData = response.data;
          localStorage.setItem('CS_US', JSON.stringify(response.data));
          //swal("Modificado", "El usuario a sido modificado correctamente", "success");
        } else {
          swal("Error", "Hubo un al modificar el usuario", "error");
        }
      }, function errorCallback(response) {
        swal("Error", "Hubo un error en la conexion con el servidor", "error");
      });
    };

    $scope.verifyPassword = function (password) {
      $scope.loandingPassword = true;
      users.verifyPassword($scope.user, password).then(function successCallback(response) {
        $scope.loandingPassword = false;
        if (response.data.status) {
          $scope.validOldPassword = true;
        } else {
          $scope.validOldPassword = false;
        }
      }, function errorCallback(response) {

      });
    };

    $scope.saveAccess = function (accessData) {
      $scope.savingAccess = true;
      users.editUserAccess($scope.user, accessData, false).then(function successCallback(response) {
        $scope.savingAccess = false;
        if (response.data.status) {
          swal("Modificado", "Su contraseña a sido modificada correctamente", "success");
          $scope.access = angular.copy({});
          $scope.formEditAccess.$setPristine();
          $scope.validOldPassword = false;
          $scope.visibleForm = false;
        } else {
          swal("Error", "Compruebe los datos proporcionados", "error");
        }
      }, function errorCallback(response) {
        swal("Error", "Error en la conexion con el servidor", "error");
      });
    };

    $scope.showFrom = function () {
      $scope.visibleForm = true;
    };

    $scope.hideFrom = function () {
      $scope.visibleForm = false;
    };

    $scope.closeForm = function () {
      $scope.access = angular.copy({});
      $scope.formEditAccess.$setPristine();
      $scope.validOldPassword = false;
      $scope.visibleForm = false;
    };

  });
