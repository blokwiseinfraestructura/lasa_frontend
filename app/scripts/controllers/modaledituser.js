'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:ModaledituserCtrl
 * @description
 * # ModaledituserCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
    .controller('ModaledituserCtrl', function ($scope, user, $uibModalInstance, users) {
        $scope.userObject = user;
        $scope.saving = false;

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.editUserModal = function (userEdit) {
            $scope.saving = true;
            users.editUser(userEdit, $scope.userObject, true).then(function successCallback(response) {
                $scope.saving = false;
                if (response.data.status) {
                    $uibModalInstance.close(response.data);
                } else {
                    swal("Error", "Error al modificar el usuario", "error");
                }
            }, function errorCallback(response) {
                swal("Error", "Hubo un error en la conexion", "error");
            });
        }
    });
