'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('LoginCtrl', function ($scope, $location, $rootScope, login) {

    $scope.loading = false;
    $scope.recoverLoading = false;

      /*if (localStorage.getItem('CS_US') !== null) {
          $location.path("/");
      }*/

    $scope.loginUsers = function (data) {
      if ($scope.formLoginUser.$valid) {
        $scope.loading = true;
        login.login(data).then(function successCallback(response) {
          console.log(response);
          if (response.data.status) {
            localStorage.setItem('CS_US', JSON.stringify(response.data));
            $rootScope.loggedInUser = 'hola';
            $location.path("/");
            location.reload();
          } else {
            swal("Error", "Compruebe sus credenciales", "error");
            $scope.loading = false;
          }
        }, function errorCallback(response) {

        });
      }
    };

    $scope.logOut = function () {
      if (localStorage.getItem('CS_US') !== null) {
        localStorage.removeItem('CS_US');
        location.reload();
      }
    };

    $scope.recoveryUser = function (userData) {
      if ($scope.formRecoveryUser.$valid) {
        $scope.recoverLoading = true;
        login.recoverPassword(userData).then(function successCallback(response) {
  console.log(response);
          $scope.recoverLoading = false;
              if (response.data.status){
                swal("Enviado", "Revice su bandeja de entrada y siga las instrucciones", "success");
                $scope.userRecover = angular.copy({});
                $scope.formRecoveryUser.$setPristine();
                $('#recoverModal').modal('hide');
              }else{
                swal("Error", "No se encontro un usuario con ese email", "error");
              }
        }, function errorCallback(response) {
          $scope.recoverLoading = false;
          swal("Error", "Ups hubo un error al hacer la peticion por favor compruebe su conexion a internet y vuelva a intentarlo", "error");
        });
      }
    };

  });
