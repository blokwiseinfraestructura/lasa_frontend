'use strict';

/**
 * @ngdoc function
 * @name lasaFrontendApp.controller:UsersappCtrl
 * @description
 * # UsersappCtrl
 * Controller of the lasaFrontendApp
 */
angular.module('lasaFrontendApp')
  .controller('UsersappCtrl', function ($scope, usersapp) {

    $scope.loanding = true;
    $scope.usersAppArray = [];

    //Valores del paginado
    $scope.maxSize = 5;
    $scope.currentPage = 4;
    $scope.pageSize = 10;

    $scope.propertyName = 'poinst';

    $scope.mySortFunction = function(item) {
      if(isNaN(item[$scope.sortExpression]))
        return item[$scope.sortExpression];
      return parseInt(item[$scope.sortExpression]);
    }

    usersapp.getAllUsers().then(function successCallback(response) {
      if (response.data.status) {
        $scope.usersAppArray = response.data.appUsersArray;
        $scope.loanding = false;
      } else {
        swal("Error", "No hay noticias creadas", "error");
        $scope.loanding = false;
      }
    }, function errorCallback(response) {
      console.log(response);
    });

    $scope.enableDisabled = function (state, userData) {

      var index = arrayObjectIndexOf($scope.usersAppArray, userData);

      swal({
        title: "Advertencia",
        text: ((state) ? "Esta seguro que desea Inhabilitar Evaluación final" : "Esta seguro que desea Habilitar Evaluación final"),
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
      }, function () {
        setTimeout(function () {
          usersapp.changeEvaluation(userData, ((state) ? false : true)).then(function successCallback(response) {
            if (response.data.status) {
              $scope.usersAppArray[index].haveFinalTestAccess = response.data.userData.haveFinalTestAccess;
              swal("Actualizado", ((state) ? "La evaluacion se a inhabilitado correctamente" : "La evaluacion se a habilitado correctamente"), "success");
            } else {
              swal("Error", "hubo un error al actualizar el registro", "error");
            }
          }, function errorCallback(response) {
            swal("Error", "Debe ingresar un total de cuatro respuestas", "error");
          });
        }, 2000);
      });
    };

    $scope.denyOrGrantAccess = function (state, userData) {

      var index = arrayObjectIndexOf($scope.usersAppArray, userData);

      swal({
        title: "Advertencia",
        text: ((state) ? "Esta seguro que desea Denegar el acceso a este usuario" : "Esta seguro que desea Habilitar el acceso a este usuario"),
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
      }, function () {
        setTimeout(function () {
          usersapp.denyOrGrantAccess(userData, ((state) ? false : true)).then(function successCallback(response) {
            if (response.data.status) {
              $scope.usersAppArray[index].access = response.data.userData.access;
              swal("Actualizado", ((state) ? "EL acceso se a denegado correctamente" : "El acceso se a habilitado correctamente"), "success");
            } else {
              swal("Error", "hubo un error al actualizar el registro", "error");
            }
          }, function errorCallback(response) {
            swal("Error", "Hubo un error al conectarse con el servidor", "error");
          });
        }, 2000);
      });
    };

    //Optener el indice de la array
    function arrayObjectIndexOf(arr, obj) {
      for (var i = 0; i < arr.length; i++) {
        if (angular.equals(arr[i], obj)) {
          return i;
        }
      }
      return -1;
    }

  });
