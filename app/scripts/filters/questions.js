'use strict';

/**
 * @ngdoc filter
 * @name lasaFrontendApp.filter:questions
 * @function
 * @description
 * # questions
 * Filter in the lasaFrontendApp.
 */
angular.module('lasaFrontendApp')
  .filter('questions', function () {
    return function(data, start){
      start = +start; //parse to int
      return data.slice(start);
    };
  });
