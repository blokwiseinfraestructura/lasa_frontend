'use strict';

/**
 * @ngdoc service
 * @name lasaFrontendApp.pushNotifications
 * @description
 * # pushNotifications
 * Factory in the lasaFrontendApp.
 */
angular.module('lasaFrontendApp')
  .factory('pushNotifications', function ($http, environment) {
    // Service logic
    // ...


    // Public API here
    return {
      addNewPushNotification: function (userData, pushData) {
        return $http({
          method: 'POST',
          url: environment + 'admin/push_notifications/create',
          headers: {
            'Content-Type': 'application/json', 'Access-control-allow-origin': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
          },
          data:    {
            userData: userData.userData,
            pushNotificationData: pushData
          }
        });
      },
      getAllNotifications: function () {
        return $http({
          method: 'GET',
          url: environment + 'admin/push_notifications',
          headers: {
            'Accept': 'application/json, text/javascript',
            'Content-Type': 'application/json; charset=utf-8'
          },
          data:{}
        });
      }
    };
  });
