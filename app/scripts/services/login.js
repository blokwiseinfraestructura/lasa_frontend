'use strict';

/**
 * @ngdoc service
 * @name lasaFrontendApp.login
 * @description
 * # login
 * Factory in the lasaFrontendApp.
 */
angular.module('lasaFrontendApp')
  .factory('login', function ($http, environment) {
    // Service logic
    // ...

    // Public API here
    return {
      login: function (data) {
        return $http({
          method: 'POST',
          url: environment + 'admin/login',
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
          data: {
            email: data.user,
            password: data.password
          }
        });
      },
      recoverPassword: function (userData) {
        return $http({
          method: 'POST',
          url: environment + 'admin/users/recover-token',
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
          data: {
            email: userData.email
          }
        });
      },
      restorePassword: function (newPassword, token, isAdmin) {
        return $http({
          method: 'POST',
          url: environment + 'admin/users/update_password',
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
          data: {
            newPassword: newPassword,
            recoverToken: token,
            isAdmin: isAdmin
          }
        });
      }
    };
  });
