'use strict';

/**
 * @ngdoc service
 * @name lasaFrontendApp.questions
 * @description
 * # questions
 * Factory in the lasaFrontendApp.
 */
angular.module('lasaFrontendApp')
    .factory('questions', function ($http, environment) {
        // Service logic
        // ...

        //var meaningOfLife = 42;

        // Public API here
        return {
            addNewQuestion: function (question, answers) {
                return $http({
                    method: 'POST',
                    url: environment + 'admin/questions/create',
                    headers: {
                        'Content-Type': 'application/json', 'Access-control-allow-origin': '*',
                        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
                    },
                    data: {
                        topic: {
                            topicId: question.topic.topicId,
                            topicName: question.topic.topicName,
                            topicCode: question.topic.topicCode
                        },
                        level: {
                            levelId: question.level.levelId,
                            levelName: question.level.levelName,
                            levelCode: question.level.levelCode
                        },
                        questionDescription: question.descripion,
                        answersArray: answers
                    }
                });
            },
            getAllQuestion: function () {
                return $http({
                    method: 'GET',
                    url: environment + 'admin/questions',
                    headers: {
                        'Accept': 'application/json, text/javascript',
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                });
            },
          deleteQuestion: function (id) {
            return $http({
              method: 'POST',
              url: environment + 'admin/questions/delete',
              headers: {
                'Accept': 'application/json, text/javascript',
                'Content-Type': 'application/json; charset=utf-8'
              },
              data:{
                questionId:id
              }
            });
          },
          editQuestion: function (question, answers, questionId) {
            return $http({
              method: 'POST',
              url: environment + 'admin/questions/edit',
              headers: {
                'Accept': 'application/json, text/javascript',
                'Content-Type': 'application/json; charset=utf-8'
              },
              data:{
                topic: question.topic,
                level:question.level,
                questionId: questionId,
                questionDescription: question.descripion,
                answersArray: answers
              }
            });
          }
        };
    });
