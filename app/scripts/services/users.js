'use strict';

/**
 * @ngdoc service
 * @name lasaFrontendApp.users
 * @description
 * # users
 * Factory in the lasaFrontendApp.
 */
angular.module('lasaFrontendApp')
    .factory('users', function ($http, environment) {
        // Service logic
        // ...


        // Public API here
        return {
            addUser: function (user, modules) {
                return $http({
                    method: 'POST',
                    url: environment + 'admin/users/register',
                    headers: {
                        'Content-Type': 'application/json', 'Access-control-allow-origin': '*',
                        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
                    },
                    data: {
                        name: user.name,
                        lastName: user.lastName,
                        email: user.email,
                        role: user.role,
                        password: user.userPassword,
                        modulesArray: modules
                    }
                });
            },
            getUser: function () {
                return $http({
                    method: 'GET',
                    url: environment + 'admin/users',
                    headers: {
                        'Content-Type': 'application/json', 'Access-control-allow-origin': '*',
                        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
                    },
                    data: {}
                });
            },
            deleteUser: function (userId) {
                return $http({
                    method: 'POST',
                    url: environment + 'admin/users/delete',
                    headers: {
                        'Content-Type': 'application/json', 'Access-control-allow-origin': '*',
                        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
                    },
                    data: {
                        userData: {
                            adminUserId: userId
                        }
                    }
                });
            },
            editUser: function (user, userObject, isAdmin) {
                return $http({
                    method: 'POST',
                    url: environment + 'admin/users/edit',
                    headers: {
                        'Content-Type': 'application/json', 'Access-control-allow-origin': '*',
                        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
                    },
                    data: {
                        isAdminEdit: isAdmin,
                        userData: {
                            adminUserId: userObject.userData.adminUserId,
                            name: user.userData.name,
                            lastName: user.userData.lastName,
                            role: user.userData.role,
                            email: user.userData.email
                        },
                        modulesArray: userObject.modulesArray
                    }
                });
            },
          verifyPassword: function (userId,password) {
            return $http({
              method: 'POST',
              url: environment + 'admin/users/password_matcher',
              headers: {
                'Content-Type': 'application/json', 'Access-control-allow-origin': '*',
                'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
              },
              data: {
                adminUserId:userId.userData.adminUserId,
                password: password
              }
            });
          },
          editUserAccess: function (userData, userAccess, isAdmin) {
            return $http({
              method: 'POST',
              url: environment + 'admin/users/edit',
              headers: {
                'Content-Type': 'application/json', 'Access-control-allow-origin': '*',
                'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
              },
              data: {
                  isAdminEdit: isAdmin,
                  userData: {
                    adminUserId: userData.userData.adminUserId,
                    name: userData.userData.name,
                    lastName: userData.userData.lastName,
                    role: userData.userData.role,
                    email: userData.userData.email,
                    password:userAccess.newPassword,
                  },
                  modulesArray: userData.modulesArray
                  }
            });
          }

        };
    });
