'use strict';

/**
 * @ngdoc service
 * @name lasaFrontendApp.readings
 * @description
 * # readings
 * Factory in the lasaFrontendApp.
 */
angular.module('lasaFrontendApp')
  .factory('readings', function ($http, Upload, environment) {
    // Service logic
    // ...


    // Public API here
    return {
      getReadings: function () {
        return $http({
          method: 'GET',
          url: environment + 'admin/readings',
          headers: {
            'Content-Type': 'application/json', 'Access-control-allow-origin': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
          },
          data: {}
        });
      },
      addReading: function (dataReading) {
        return dataReading.upload = Upload.upload({
          url: environment + 'admin/readings/create',
          data: {
            file: dataReading.image,
            readingDescription: dataReading.description,
            topicId: dataReading.topic.topicId,
            levelId: dataReading.level.levelId
          }
        });
      },
      editReading: function (dataReading, readingId) {
        return dataReading.upload = Upload.upload({
          url: environment + 'admin/readings/edit',
          data: {
            readingId: readingId,
            file: dataReading.image,
            readingDescription: dataReading.description,
            topicId: dataReading.topic.topicId,
            levelId: dataReading.level.levelId
          }
        });
      },
      deleteReading: function (dataReading) {
        return $http({
          method: 'POST',
          url: environment + 'admin/readings/delete',
          headers: {
            'Content-Type': 'application/json', 'Access-control-allow-origin': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
          },
          data: {
            reading: {
              readingId: dataReading.readingId
            }
          }
        });
      }
    };
  });
