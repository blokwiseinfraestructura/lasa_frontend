'use strict';

/**
 * @ngdoc service
 * @name lasaFrontendApp.news
 * @description
 * # news
 * Factory in the lasaFrontendApp.
 */
angular.module('lasaFrontendApp')
  .factory('news', function ($http, Upload, environment) {
    // Service logic
    // ...

    // Public API here
    return {
      addNews: function (dataNews) {
        return dataNews.upload = Upload.upload({
          url: environment + 'admin/news/create',
          data: {
            newsTitle: dataNews.title,
            newsDescription: dataNews.description,
            imageFile: dataNews.image
          }
        });
      },
      getNews: function () {
        return $http({
          method: 'GET',
          url: environment + 'admin/news',
          headers: {
            'Content-Type': 'application/json', 'Access-control-allow-origin': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
          },
          data: {}
        });
      },
      deleteNews: function (newsData) {
        return $http({
          method: 'POST',
          url: environment + 'admin/news/delete',
          headers: {
            'Content-Type': 'application/json', 'Access-control-allow-origin': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
          },
          data: {
            news: {
              newsId: newsData.newsId,
              newsTitle: newsData.newsTitle,
              newsDescription: newsData.newsDescription,
              newsImagePath: newsData.newsImagePath,
              createAt: newsData.createAt
            }
          }
        });
      },
      editDataNews: function (newsFormData, newsData ) {
        return newsFormData.upload = Upload.upload({
          url: environment + 'admin/news/edit',
          data: {
            newsId:newsData.newsId,
            newsTitle: newsFormData.title,
            newsDescription: newsFormData.description,
            imageFile: newsFormData.image
          }
        });
      }
    };
  });
