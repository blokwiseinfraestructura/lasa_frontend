'use strict';

/**
 * @ngdoc service
 * @name lasaFrontendApp.usersapp
 * @description
 * # usersapp
 * Factory in the lasaFrontendApp.
 */
angular.module('lasaFrontendApp')
  .factory('usersapp', function ($http, environment) {
    // Service logic
    // ...


    // Public API here
    return {
      getAllUsers: function () {
        return $http({
          method: 'GET',
          url: environment + 'admin/app_users',
          headers: {
            'Accept': 'application/json, text/javascript',
            'Content-Type': 'application/json; charset=utf-8'
          },
          data:{}
        });
      },
      getUserDetails: function (userData) {
        return $http({
          method: 'POST',
          url: environment + 'admin/app_users/detail',
          headers: {
            'Accept': 'application/json, text/javascript',
            'Content-Type': 'application/json; charset=utf-8'
          },
          data:{
            userId: userData.userId
          }
        });
      },
      changeEvaluation: function (userData, state) {
        return $http({
          method: 'POST',
          url: environment + 'admin/app_users/final_test_permission',
          headers: {
            'Accept': 'application/json, text/javascript',
            'Content-Type': 'application/json; charset=utf-8'
          },
          data:{
            userData: userData,
            enableFinalTest: state
          }
        });
      },
      sendEmail: function (emailData, userData) {
        return $http({
          method: 'POST',
          url: environment + 'admin/app_users/send_custom_email',
          headers: {
            'Accept': 'application/json, text/javascript',
            'Content-Type': 'application/json; charset=utf-8'
          },
          data:{
            emailSubject: emailData.affair,
            emailBody: emailData.message,
            email: userData.email
          }
        });
      },
      denyOrGrantAccess: function (userData, state) {
        return $http({
          method: 'POST',
          url: environment + 'admin/app_users/deny-or-grant-access',
          headers: {
            'Accept': 'application/json, text/javascript',
            'Content-Type': 'application/json; charset=utf-8'
          },
          data:{
            userId: userData.userId,
            access: state
          }
        });
      }
    };
  });
