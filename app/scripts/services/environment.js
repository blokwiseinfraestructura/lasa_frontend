'use strict';

/**
 * @ngdoc service
 * @name lasaFrontendApp.environment
 * @description
 * # environment
 * Constant in the lasaFrontendApp.
 */
angular.module('lasaFrontendApp')
  .constant('environment', 'http://localhost/proyectos-bwstudios/lasa_app_backend/web/app_dev.php/api/');

//pre poduccion
// http://lasaappbackend.azurewebsites.net/web/app.php/api/

// produccion
//http://190.144.20.10:8082/lasa_app_backend/web/api/

//Local
// http://localhost/proyectos-bwstudios/lasa_app_backend/web/api/
// http://localhost/proyectos-bwstudios/lasa_app_backend/web/app_dev.php/api/
