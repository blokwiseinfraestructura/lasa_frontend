'use strict';

/**
 * @ngdoc overview
 * @name lasaFrontendApp
 * @description
 * # lasaFrontendApp
 *
 * Main module of the application.
 */
angular
  .module('lasaFrontendApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'xeditable',
    'ngTable',
    'ngFileUpload'
  ])
  .config(function ($routeProvider, $locationProvider) {//$locationProvider Desactivar #! de las rutas
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/questions', {
        templateUrl: 'views/questions.html',
        controller: 'QuestionsCtrl',
        controllerAs: 'questions'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/loader', {
        templateUrl: 'views/loader.html',
        controller: 'LoaderCtrl',
        controllerAs: 'loader'
      })
      .when('/users', {
        templateUrl: 'views/users.html',
        controller: 'UsersCtrl',
        controllerAs: 'users'
      })
      .when('/recover/:token/:isAdmin', {
        templateUrl: 'views/recover.html',
        controller: 'RecoverCtrl',
        controllerAs: 'recover'
      })
      .when('/profile', {
        templateUrl: 'views/profile.html',
        controller: 'ProfileCtrl',
        controllerAs: 'profile'
      })
      .when('/news', {
        templateUrl: 'views/news.html',
        controller: 'NewsCtrl',
        controllerAs: 'news'
      })
      .when('/pushNotifications', {
        templateUrl: 'views/pushnotifications.html',
        controller: 'PushnotificationsCtrl',
        controllerAs: 'pushNotifications'
      })
      .when('/usersApp', {
        templateUrl: 'views/usersapp.html',
        controller: 'UsersappCtrl',
        controllerAs: 'usersApp'
      })
      .when('/usersAppDetail/:userId', {
        templateUrl: 'views/usersappdetail.html',
        controller: 'UsersappdetailCtrl',
        controllerAs: 'usersAppDetail'
      })
      .when('/readings', {
        templateUrl: 'views/readings.html',
        controller: 'ReadingsCtrl',
        controllerAs: 'readings'
      })
      .when('/modalEditReadings', {
        templateUrl: 'views/modaleditreadings.html',
        controller: 'ModaleditreadingsCtrl',
        controllerAs: 'modalEditReadings'
      })
      .otherwise({
        redirectTo: '/'
      });
    /*$locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });//$locationProvider Desactivar #! de las rutas*/
  }).run(function ($rootScope, $location) {
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
      if (localStorage.getItem('CS_US') !== null) {
        $rootScope.loggedInUser = 'hola';
        $rootScope.userData = JSON.parse(localStorage.getItem('CS_US'));

      }else {
        $rootScope.loggedInUser = null;
      }
      if ($rootScope.loggedInUser === null){
        if ( next.templateUrl === "views/login.html") {
        } else if (next.templateUrl === "views/recover.html"){

        } else if (next.templateUrl === "views/loader.html") {

        }else {
          $location.path("/login");
        }
      }
    });
}).run(function(editableOptions) {
  editableOptions.theme = 'bs3';
});
